.PHONY: clean

slides.pdf: main.tex
	xelatex main.tex 
	mv main.pdf slides.pdf && okular slides.pdf

clean:
	rm *.aux *.log *.out *.snm || true

realclean: clean
	rm slides.pdf *.toc *.nav
